# API empresas
Este *README* foi dividido nos seguintes tópicos:

  - **Estrutura deste repositório**
  
  - **Banco de dados**

  - **Autenticação**
  
  - **Como exeutar esta aplicação**

  - **Fonte de dados**
  
  - **Testes**

  - **Considerações finais e resultados**

### Estrutura deste repositório:
Cada branch desta API é desenhada para consumir o banco de dados de seu respectivo nome, portanto: a branch SQLServer consome um banco de dados SQLServer hospedado em um servidor Azure; a branch MySQL consome uma instância MySQL na Google Cloud Platform.

### Banco de dados:
A organização das relações do banco de dados desta aplicação é apresentada na imagem abaixo.

![Relação](https://www.dropbox.com/s/eigbmw1yfspc1sz/Capturar.PNG?dl=1)

### Autenticação
Tendo como base a especificação RFC 6749 fora implementado o método de autenticação OAuth 2.0 fazendo uso de JWT (RFC 7519) como tokens para encorpar a segurança da sistema.

### Como executar esta aplicação
Esta aplicação não requer obrigatoriamente uma instância de banco de dados para ser executada devido a já possuírem sistemas dedicados em nuvem para tanto. Contudo, pode-se executa-la utilizando um banco de dados local fazendo as devidas alterações na string de conexão presente no arquivo appsettings.json e clonando a branch referente ao BD escolhido. Além disso, recomenda-se executar ```update-database``` no console para executar o script de preenchimento dos dados iniciais no banco de dados.
	
### Fonte de dados
A fonte de dados utilizada para popular as tabelas desta aplicação é na verdade um script que consome a API de exemplo para replicar seus records no banco de dados da aplicação.

### Testes
Foi incluído neste repositório na branch master um arquivo de testes do Postman, feito nos mesmos moldes do arquivo Postman fornecido porém adaptado ao método de autenticação implementado.

### Considerações finais e resultados
Fazendo uso das documentações e instruções disponíveis foi possível desenvolver esta API de modo a estar nos conformes à especificação proposta, fazendo a implementação de todas as endpoints propostas.